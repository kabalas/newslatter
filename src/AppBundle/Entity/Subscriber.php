<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
namespace AppBundle\Entity;
/**
 * Description of Subscriber
 *
 * @author kabal
 */
class Subscriber {
    protected $userName;
    protected $userEmail;
    protected $selectedCategories;
    protected $oldEmail;
    
    public function getUserName(){
        return $this->userName;
    }
    
    public function setUserName($userName){
        $this->userName = $userName;
    }
    
    public function getUserEmail(){
        return $this->userEmail;
    }
    
    public function setUserEmail($userEmail){
        $this->userEmail = $userEmail;
    }
    
    public function getSelectedCategories(){
        return $this->selectedCategories;
    }
    
    public function setSelectedCategories($selectedCategories){
        $this->selectedCategories = $selectedCategories;
    }
    
    public function getOldEmail(){
        return $this->oldEmail;
    }
    
    public function setOldEmail($oldEmail){
        $this->oldEmail = $oldEmail;
    }
       
    
    
}
