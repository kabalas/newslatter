<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace AppBundle\Entity;

/**
 * Description of Categories
 *
 * @author kabal
 */
class Categories {
    public function getCategories(){
       
        $categoriesToPass = [];
        $categories = simplexml_load_file('../categories.xml');
        foreach ($categories as $category){   
            $categoriesToPass[(string)$category->name] =(int)$category->id;
        }
        return $categoriesToPass;
    }
    
    public function getCategoriesForAdmin(){
        $categoriesToPass = [];
        $categories = simplexml_load_file('../categories.xml');
        foreach ($categories as $category){   
            $categoriesToPass[(int)$category->id] =(string)$category->name;
        }
        return $categoriesToPass;
    }
}
