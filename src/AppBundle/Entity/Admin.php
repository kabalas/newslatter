<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace AppBundle\Entity;

/**
 * Description of Admin
 *
 * @author kabal
 */
class Admin {
    protected $adminName;
    protected $password;
    
    public function getAdminName(){
        return $this->adminName;
    }
    
    public function setAdminName($adminName){
        $this->adminName = $adminName;
    }
    
    public function getPassword(){
        return $this->password;
    }
    
    public function setPassword($password){
        $this->password = $password;
    }
}
