<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace AppBundle\Controller;


use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use AppBundle\Entity\Admin;
use AppBundle\Entity\Categories;
use AppBundle\Entity\Check;
use AppBundle\Entity\Subscriber;

/**
 * Description of AdminController
 *
 * @author kabal
 */
class AdminController extends Controller{
    /**
     * @Route("/admin/")
     */
    public function adminAction(Request $request) {
        
        $admin = new Admin();
        
        $form = $this->createFormBuilder($admin)
                ->add('adminName', TextType::class,array('attr'=> array('class'=>'form-control')))
                ->add('password', \Symfony\Component\Form\Extension\Core\Type\PasswordType::class,array('attr'=> array('class'=>'form-control')))
                ->add('login', SubmitType::class, array('label'=>'Login'))
                ->getForm();
        $form->handleRequest($request);
        
        if($form->isSubmitted() && $form->isValid()){
            print_r($form->getData());
            if($admin->getAdminName() == 'admin' && $admin->getPassword() == '1234'){
                return $this->redirectToRoute('panel');
            }else{
                return $this->redirectToRoute('admin');
            }
            
        }
        return $this->render('adminpanel/admin.html.twig', array(
            'form' => $form->createView(),
        ));
        
    }
    
    /**
     * @Route("/panel/")
     */
    
    public function panelAction(){
        $categories = new Categories();
        return $this->render('adminpanel/panel.html.twig', array(
            'users' => simplexml_load_file('../users.xml'),
            'categories' => $categories->getCategoriesForAdmin(),
        ));
    }
    
    public function editAction($email){
        $userName = '';
        $subscriber = new Subscriber();
        $xml = simplexml_load_file('../Newslatter/users.xml');
        foreach($xml as $user){
            //print_r($user->user_email);
            if((string)$user->user_email == $email){
                $userName = (string)$user->user_name;
                
            }  
        }
        
        
        
        $form = $this->createFormBuilder($subscriber)
                ->setAction($this->generateUrl('update'))
                ->setMethod('POST')
                ->add('userName', TextType::class, array('data' => $userName,'attr'=> array('class'=>'form-control')))
                ->add('userEmail', EmailType::class, array('data' => $email,'attr'=> array('class'=>'form-control')))
                ->add('oldEmail', \Symfony\Component\Form\Extension\Core\Type\HiddenType::class, array('data' => $email))
                ->add('save', SubmitType::class, array('label'=>'Save Subscriber'))
                ->getForm();
        //$form->handleRequest($request);
        
        /*if($form->isSubmitted() && $form->isValid()){
            print_r($form->getData());   
                return $this->redirectToRoute('panel');
            
        }*/
        
        
        return $this->render('adminpanel/edit.html.twig', array(
            
            'form' => $form->createView(),
        ));
    }
    
    public function removeAction($email){
        $xml = simplexml_load_file('../users.xml');
        foreach($xml as $user){
            //print_r($user->user_email);
            if((string)$user->user_email == $email){
                $dom = dom_import_simplexml($user);
                $dom->parentNode->removeChild($dom);
                
                unset($user);
                echo $xml->asXML();
               $xml->asXML('../users.xml');
            }  
        }
        
     return $this->redirectToRoute('panel');
    }
    
    public function updateAction(Request $request){
        $check = new Check();
        $params = $request->request->get('form');
        echo $params['userName'];
        if($check->checkEmail($params['userEmail'])){
            $xml = simplexml_load_file('http://127.0.0.1/Newslatter/users.xml');
             foreach($xml as $user){
                 //print_r($user->user_email);
                 if((string)$user->user_email == $params['oldEmail']){
                    $user->user_email = $params['userEmail'];
                    $user->user_name = $params['userName'];
                    $xml->asXML('../users.xml');
                     return $this->redirectToRoute('panel');
                 }  
             }
        }
    
    }
}
