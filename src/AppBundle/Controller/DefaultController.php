<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RedirectResponse;
use AppBundle\Entity\Subscriber;
use AppBundle\Entity\Categories;
use AppBundle\Entity\Check;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;

class DefaultController extends Controller
{
    /**
     * @Route("/", name="homepage")
     */
    public function indexAction(Request $request)
    {
        
        $subscriber = new Subscriber();
        $categories = new Categories();
        
        $form = $this->createFormBuilder($subscriber)
                ->add('userName', TextType::class, array('attr'=> array('class'=>'form-control')))
                ->add('userEmail', EmailType::class, array('attr'=> array('class'=>'form-control')))
                ->add('selectedCategories', ChoiceType::class,[
                    'choices' => $categories->getCategories(),
                    'multiple' => true,
                    'attr'=> array('class'=>'form-control')
                    
                ])
                ->add('save', SubmitType::class, array('label'=>'Save Subscriber'))
                ->getForm();
        
        $form->handleRequest($request);
        
        if($form->isSubmitted() && $form->isValid()){
            if($this->create($subscriber)){
                return $this->redirectToRoute('succses');
            }
                
             return $this->render('default/index.html.twig',array(
            'form' => $form->createView(),
               ));
        }
        //return $this->redirect('admin');
        // replace this example code with whatever you need
        return $this->render('default/index.html.twig',array(
            'form' => $form->createView(),
        ));
    }
    
    /**
     * @Route("/succses/")
     */
    
    public function succsesAction(){
        return $this->render('default/succses.html.twig');
    }
    
    public function create($newUser){
        $check = new Check;
        if($check->checkEmail($newUser->getUserEmail())){
            $xml = simplexml_load_file('../users.xml');
            $user = $xml->addChild('user');
            $user->addChild('date', date("Y-m-d"));
            $user->addChild('user_name', $newUser->getUserName());
            $user->addChild('user_email', $newUser->getUserEmail());
            $cats = $user->addChild('categories');
            foreach ($newUser->getSelectedCategories() as $cat){
                $cats->addChild('category', $cat);
            }
            $xml->asXML('../users.xml');
            return true;
        }
        return false;
        
        
    }
}
